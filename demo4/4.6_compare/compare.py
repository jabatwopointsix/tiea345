# base.py to start all the projects
# repository also includes the testing module
#!/usr/bin/env python

#mukaillen käytetty: https://pythonprogramming.net/feature-matching-homography-python-opencv-tutorial/

#########################
#       IMPORTS         #
#########################

import os
import datetime
import time
import argparse
import sys
import cv2
import numpy as np
#import matplotlib.pyplot as plt

#########################
#   GLOBAL VARIABLES    #
#########################

running_parameters = {
    'verbose':      False,
    'message':      "",
    'image1':		"pic1.png",
    'image2':		"pic2.png"
}

#########################
#   ARGUMENT PARSERS    #
#########################

# prints if verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints the running parameters
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

# parses the arguments
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    # stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    # stores message
    parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    # parses arguments
    args = parser.parse_args()

    # parses message argument
    if(args.message):
        running_parameters["message"] = str(args.message)

    # parses verbosity argument
    if(args.verbose):
        running_parameters["verbose"] = True


#########################
#   MAIN PROGRAMM       #
#########################

if __name__ == "__main__":
    # PARSE ARGUMENTS STARTS
    args = parseArguments(sys.argv)
    printParameters()
    # ARGUMENT PARSE ENDS

    img1 = cv2.imread("pic1.png",0)
    img2 = cv2.imread("pic2.png",0)

    orb = cv2.ORB_create()

    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck = True)

    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x:x.distance)

    img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10],None, flags=2)
    cv2.imwrite("result.jpg", img3)

    #EXIT
    sys.exit(0)