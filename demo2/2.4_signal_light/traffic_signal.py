#!/usr/bin/python
import sys
import time
import argparse
import RPi.GPIO as GPIO

#########################
#	GLOBAL VARIABLES	#
#########################

#output pin for the physical devices

# CAR TRAFFIC
CAR_RED_LED_OUTPUT_PIN          = 17
CAR_YELLOW_LED_OUTPUT_PIN       = 27
CAR_GREEN_LED_OUTPUT_PIN        = 22

# WALKING TRAFFIC
WALKING_RED_LED_OUTPUT_PIN      = 24
WALKING_GREEN_LED_OUTPUT_PIN    = 23

# BUTTON 
BUTTON_INPUT_PIN                = 10

# MOTION SENSOR
MOTION_SENSOR_INPUT_PIN         = 7

# SIGNAL LED
SIGNAL_LED_OUTPUT_PIN           = 18

# STATE OF TRAFFIC LIGHTS, FOR FUTURE USE

# 0 = RED
# 1 = NOT IN USE, HERE FOR SIMPLISITY
# 2 = GREEN
WALKING_LIGHT_STATE             = 2

# 0 = RED
# 1 = YELLOW
# 2 = GREEN
CAR_TRAFFIC_LIGHT_STATE         = 0

#########################
#	INIT COMMANDS		#
#########################

# setting up the gpio pins
GPIO.setmode(GPIO.BCM)

#Setting up each pin
GPIO.setup(CAR_RED_LED_OUTPUT_PIN,GPIO.OUT)
GPIO.setup(CAR_YELLOW_LED_OUTPUT_PIN,GPIO.OUT)
GPIO.setup(CAR_GREEN_LED_OUTPUT_PIN,GPIO.OUT)
GPIO.setup(WALKING_RED_LED_OUTPUT_PIN,GPIO.OUT)
GPIO.setup(WALKING_GREEN_LED_OUTPUT_PIN,GPIO.OUT)
GPIO.setup(BUTTON_INPUT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(MOTION_SENSOR_INPUT_PIN,GPIO.IN)
GPIO.setup(SIGNAL_LED_OUTPUT_PIN,GPIO.OUT)

#for later use in order to feed in parameters
running_parameters = {
    'verbose':      False,
}

#########################
#	LED FUNCTIONS 	    #
#########################

#blinks led for number of seconds feed in as parameters
def blinkLed(numOfPin, howLongInSeconds):
	#sets the led on
	GPIO.output(numOfPin,GPIO.HIGH)
	#waits so that the lead stays on
	time.sleep(howLongInSeconds)
	#sets the led off
	GPIO.output(numOfPin,GPIO.LOW)
	#waits for a few seconds again in order the led to stay off
	time.sleep(howLongInSeconds)

def ledOn(numOfPin):
	GPIO.output(numOfPin,GPIO.HIGH)

def ledOff(numOfPin):
	GPIO.output(numOfPin,GPIO.LOW)

#performs cleanup when program quits
def performCleanUp():
	GPIO.cleanup()

def turnAllLedsOff():
    ledOff(CAR_RED_LED_OUTPUT_PIN)
    ledOff(CAR_YELLOW_LED_OUTPUT_PIN)
    ledOff(CAR_GREEN_LED_OUTPUT_PIN)
    ledOff(WALKING_GREEN_LED_OUTPUT_PIN)
    ledOff(WALKING_RED_LED_OUTPUT_PIN)
    ledOff(SIGNAL_LED_OUTPUT_PIN)



#########################
#   MOTION FUNCTIONS    #
#########################

def motionSensor():
    motion_state = 0
    timer = 2
    index = 0
    print("Messuring motion for %i seconds" % timer) 
    while True:
        motion_state = GPIO.input(MOTION_SENSOR_INPUT_PIN)
        #print(motion_state)
        time.sleep(timer)
        if(index == timer):
            return motion_state
        else:
            index = index + 1

#########################
#	CMD-Argument parse	#
#########################

#for debunginf when verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints running parameters when verbosity is set on
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

#parses arguments when the software is running
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    #stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    #stores message
    #parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    args = parser.parse_args()

    #if(args.message):
    #    running_parameters["message"] = str(args.message)

    if(args.verbose):
        running_parameters["verbose"] = True



#########################
#   TRAFFIC LIGHTS      #
#########################

#changes the car traffic lights from red to green
def changeCarLightsToGreen():
    ledOff(CAR_RED_LED_OUTPUT_PIN)
    ledOn(CAR_YELLOW_LED_OUTPUT_PIN)
    time.sleep(1)
    ledOff(CAR_YELLOW_LED_OUTPUT_PIN)
    ledOn(CAR_GREEN_LED_OUTPUT_PIN)

#changes the car traffic lights from green to red#ch
def changeCarLightsToRed():
    ledOff(CAR_GREEN_LED_OUTPUT_PIN)
    ledOn(CAR_YELLOW_LED_OUTPUT_PIN)
    time.sleep(1)
    ledOff(CAR_YELLOW_LED_OUTPUT_PIN)
    ledOn(CAR_RED_LED_OUTPUT_PIN)

#changes the walking lights from green to red
def changeWalkingLightsToRed():
    ledOff(WALKING_GREEN_LED_OUTPUT_PIN)
    ledOn(WALKING_RED_LED_OUTPUT_PIN) 

#changes the walking lights from red to green 
def changeWalkingLightsToGreen():
    ledOff(WALKING_RED_LED_OUTPUT_PIN)
    ledOn(WALKING_GREEN_LED_OUTPUT_PIN)

# is executed when button is pressed, has the logic built in
def buttonWasPressed():
    ledOn(SIGNAL_LED_OUTPUT_PIN)
    time.sleep(2)
    changeCarLightsToRed()
    time.sleep(0.5)
    ledOff(SIGNAL_LED_OUTPUT_PIN)
    changeWalkingLightsToGreen()
    time.sleep(10)
    changeWalkingLightsToRed()
    time.sleep(0.5)
    changeCarLightsToGreen()
    time.sleep(2)

#########################
#	Main program		#
#########################

#main section of the program
if __name__ == "__main__":
    args = parseArguments(sys.argv)
    printParameters()

    turnAllLedsOff()
    ledOn(CAR_GREEN_LED_OUTPUT_PIN)
    ledOn(WALKING_RED_LED_OUTPUT_PIN)
    
    #button car ligts red --> green, walking lights red to green
    

    try:
        while True:
            #button is pressed
            if (GPIO.input(BUTTON_INPUT_PIN) == False ):
                buttonWasPressed()

    except KeyboardInterrupt:
        print(" User quit with keyboard")
    
    
    
    performCleanUp()
    sys.exit(0)

