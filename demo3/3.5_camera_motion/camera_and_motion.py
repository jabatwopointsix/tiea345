#!/usr/bin/python
import sys
import time
from picamera import PiCamera
import argparse
import RPi.GPIO as GPIO

#########################
#	GLOBAL VARIABLES	#
#########################

#output pin for the physical devices
MOTION_SENSOR_INPUT_PIN = 7

CAMERA = PiCamera()

#########################
#	INIT COMMANDS		#
#########################

# setting up the gpio pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(MOTION_SENSOR_INPUT_PIN,GPIO.IN) 

#for later use in order to feed in parameters
running_parameters = {
    'verbose':      False,
}

#########################
#	Program starts		#
#########################

def motionSensor():
    motion_state = 0
    timer = 2
    index = 0
    #GPIO.setup(MOTION_SENSOR_INPUT_PIN,GPIO.IN)   
    print("Messuring motion for %i seconds" % timer) 
    while True:
        motion_state = GPIO.input(MOTION_SENSOR_INPUT_PIN)
        #print(motion_state)
        time.sleep(timer)
<<<<<<< HEAD
        if(index == timer):
            return motion_state
        else:
            index = index + 1
=======

>>>>>>> bd2fcb1d469b0d2990aba35816387c08166628d5

#performs cleanup when program quits
def performCleanUp():
	GPIO.cleanup()


#########################
#   CAMERA functions    #
#########################

def takeStill(path):
    CAMERA.resolution = (2592, 1944)
    CAMERA.rotation = 180
    CAMERA.capture(path)

def recordVideo(path,dur):
    CAMERA.resolution = (1280, 720)
    CAMERA.rotation = 180
    CAMERA.start_recording(path)
    time.sleep(dur)
    CAMERA.stop_recording()


#########################
#	CMD-Argument parse	#
#########################

#for debunginf when verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints running parameters when verbosity is set on
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

#parses arguments when the software is running
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    #stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    #stores message
    #parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    args = parser.parse_args()

    #if(args.message):
    #    running_parameters["message"] = str(args.message)

    if(args.verbose):
        running_parameters["verbose"] = True

#########################
#	Main program		#
#########################

#main section of the program
if __name__ == "__main__":
    args = parseArguments(sys.argv)
    printParameters()
<<<<<<< HEAD
    #ledOff(LED_OUTPUT_PIN)
    #motionSensor()
    try:
        recordVideo("video.h264",15)
        print("video done")
    except KeyboardInterrupt:
         print(" User quit with keyboard")

    try:
        takeStill("image.jpg")
        print("picture done")
    except KeyboardInterrupt:
        print(" User quit with keyboard")

    motion_sensor_state = motionSensor()

    if(motion_sensor_state == 1):
        print("motion was detected")
    else:
        print("motion was not detected")
=======
    index = 0;
    while True:
        motion_state = GPIO.input(MOTION_SENSOR_INPUT_PIN)
        if(motion_state != 0):
            takeStill("pic%i.jpg" % index)
            index = index + 1
        #print(motion_state)
        time.sleep(1)

>>>>>>> bd2fcb1d469b0d2990aba35816387c08166628d5
    
    #performs the quit functions
    performCleanUp()
    sys.exit(0)

