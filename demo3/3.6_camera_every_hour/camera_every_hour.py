#!/usr/bin/python
import sys
import time
import datetime
from picamera import PiCamera
import argparse
import RPi.GPIO as GPIO

#########################
#	GLOBAL VARIABLES	#
#########################

#output pin for the physical devices
MOTION_SENSOR_INPUT_PIN = 7

CAMERA = PiCamera()

#########################
#	INIT COMMANDS		#
#########################

# setting up the gpio pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(MOTION_SENSOR_INPUT_PIN,GPIO.IN) 

#for later use in order to feed in parameters
running_parameters = {
    'verbose':      False,
}

#########################
#	Program starts		#
#########################

def motionSensor():
    motion_state = 0
    timer = 2
    index = 0
    #GPIO.setup(MOTION_SENSOR_INPUT_PIN,GPIO.IN)   
    print("Messuring motion for %i seconds" % timer) 
    while True:
        motion_state = GPIO.input(MOTION_SENSOR_INPUT_PIN)
        #print(motion_state)
        time.sleep(timer)
        if(index == timer):
            return motion_state
        else:
            index = index + 1

#performs cleanup when program quits
def performCleanUp():
	GPIO.cleanup()


#########################
#   CAMERA functions    #
#########################

def takeStill(path):
    CAMERA.resolution = (2592, 1944)
    CAMERA.rotation = 180
    CAMERA.capture(path)

def recordVideo(path,dur):
    CAMERA.resolution = (1280, 720)
    CAMERA.rotation = 180
    CAMERA.start_recording(path)
    time.sleep(dur)
    CAMERA.stop_recording()


#########################
#	CMD-Argument parse	#
#########################

#for debunginf when verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints running parameters when verbosity is set on
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

#parses arguments when the software is running
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    #stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    #stores message
    #parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    args = parser.parse_args()

    #if(args.message):
    #    running_parameters["message"] = str(args.message)

    if(args.verbose):
        running_parameters["verbose"] = True

#########################
#	Main program		#
#########################

# TIMING STUFF

#checks if it is midnight right now
def midNight(datetimer):
    if(datetimer.second == 0 and datetimer.minute == 0 and datetimer.hour == 0):
        return True
    else:
        return False

#Every ten minutes returns true
def everyTenMinutes(datetimer):
    if(datetimer.second == 0 and datetimer.minute % 10 == 0):
        return True
    else:
        return False

#Every exact minute returns true
def everyMinute(datetimer):
    if(datetimer.second == 0):
        return True
    else:
        return False

#Every hour returns true
def everyHour(datetimer):
    if(datetimer.second == 0 and datetimer.minute == 0):
        return True
    else:
        return False

#LOGGERSTUFF
def timeNowStr():
    return "" + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

#main section of the program
if __name__ == "__main__":
    args = parseArguments(sys.argv)
    printParameters()
    #ledOff(LED_OUTPUT_PIN)
    #picsStr = timeNowStr() + "_pic.jpg"
    #takeStill("/var/www/html/pics/" + picsStr)
 
    while(True):
        time.sleep(1)
        datetimer = datetime.datetime.now()
        if(everyHour(datetimer)):
            picsStr = timeNowStr() + "_pic.jpg"
            takeStill("/var/www/html/pics/" + picsStr)

 
    #performs the quit functions
    performCleanUp()
    sys.exit(0)

