#!/usr/bin/python
import sys
import time
import datetime
import argparse
import RPi.GPIO as GPIO
# https://github.com/szazo/DHT11_Python - Found this and it appeared to work just fine and was not very complicated
import dht11
import gspread
from oauth2client.service_account import ServiceAccountCredentials
 
#Create connection to the google sheats
scope = ['https://spreadsheets.google.com/feeds']
creds = ServiceAccountCredentials.from_json_keyfile_name('tiea345-tempsensor.json', scope)
client = gspread.authorize(creds)

#create global that has all the data
SHEET = client.open("tempsensor").sheet1



#########################
#	GLOBAL VARIABLES	#
#########################

#output pin for the physical devices
TEMP_INPUT_PIN = 10


#########################
#	INIT COMMANDS		#
#########################

# setting up the gpio pins
GPIO.setmode(GPIO.BCM)


#for later use in order to feed in parameters
running_parameters = {
    'verbose':      False,
}

#########################
#	Program starts		#
#########################

#performs cleanup when program quits
def performCleanUp():
	GPIO.cleanup()


#########################
#	CMD-Argument parse	#
#########################

#for debunginf when verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints running parameters when verbosity is set on
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

#parses arguments when the software is running
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    #stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    #stores message
    #parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    args = parser.parse_args()

    #if(args.message):
    #    running_parameters["message"] = str(args.message)

    if(args.verbose):
        running_parameters["verbose"] = True

#########################
#	Main program		#
#########################

#main section of the program
if __name__ == "__main__":
    args = parseArguments(sys.argv)
    printParameters()
    print(SHEET.row_count)
    SHEET.resize(SHEET.row_count)
    try:
        while True:
            sensor = dht11.DHT11(pin=TEMP_INPUT_PIN)
            result = sensor.read()
            if result.is_valid():
                #print("Temperature: %d C" % result.temperature)
                #print("Humidity: %d %%" % result.humidity)
                row = [datetime.datetime.now(), result.temperature, result.humidity]
                SHEET.append_row(row)
            time.sleep(1)
    except:
        print(" user quitted process")

    # #performs the quit functions
    # #performCleanUp()
    sys.exit(0)

