#!/usr/bin/python
import sys
import time
import argparse
import RPi.GPIO as GPIO

#########################
#	GLOBAL VARIABLES	#
#########################

#output pin for the red led
OUTPUTPIN = 27

#########################
#	INIT COMMANDS		#
#########################

# setting up the gpio pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(OUTPUTPIN,GPIO.OUT)

#for later use in order to feed in parameters
running_parameters = {
    'verbose':      False,
}

#########################
#	Program starts		#
#########################

#blinks led for number of seconds feed in as parameters
def blinkLed(numOfPin, howLongInSeconds):
	#sets the led on
	GPIO.output(numOfPin,GPIO.HIGH)
	#waits so that the lead stays on
	time.sleep(howLongInSeconds)
	#sets the led off
	GPIO.output(numOfPin,GPIO.LOW)
	#waits for a few seconds again in order the led to stay off
	time.sleep(howLongInSeconds)

#performs cleanup when program quits
def performCleanUp():
	GPIO.cleanup()


#########################
#	CMD-Argument parse	#
#########################

#for debunginf when verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints running parameters when verbosity is set on
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

#parses arguments when the software is running
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    #stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    #stores message
    #parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    args = parser.parse_args()

    #if(args.message):
    #    running_parameters["message"] = str(args.message)

    if(args.verbose):
        running_parameters["verbose"] = True

#########################
#	Main program		#
#########################

#main section of the program
if __name__ == "__main__":
    args = parseArguments(sys.argv)
    printParameters()

    #blinks the led for three times
    for i in range(1,3+1):
    	#17 = the output pin

    	blinkLed(OUTPUTPIN,1)
   
    #performs the quit functions
    performCleanUp()
    sys.exit(0)

