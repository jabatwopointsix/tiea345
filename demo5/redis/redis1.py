# base.py to start all the projects
# repository also includes the testing module
#!/usr/bin/env python

#########################
#       IMPORTS         #
#########################

import os
import datetime
import time
import argparse
import sys
import redis

#########################
#   GLOBAL VARIABLES    #
#########################

running_parameters = {
    'verbose':      False,
    'message':      "",
}

#########################
#   ARGUMENT PARSERS    #
#########################

# prints if verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints the running parameters
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

# parses the arguments
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    # stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    # stores message
    parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    # parses arguments
    args = parser.parse_args()

    # parses message argument
    if(args.message):
        running_parameters["message"] = str(args.message)

    # parses verbosity argument
    if(args.verbose):
        running_parameters["verbose"] = True


#########################
#   MAIN PROGRAMM       #
#########################

if __name__ == "__main__":
    # PARSE ARGUMENTS STARTS
    args = parseArguments(sys.argv)
    printParameters()
    # ARGUMENT PARSE ENDS

    r_server = redis.Redis("localhost")

    index = 0

    while(True):
        # välitetään viestiä
        r_server.set("topic",index)
        index = index + 1
        time.sleep(1)


    #EXIT
    sys.exit(0)